'''
Created on Jun 9, 2014

@author: hauyeung
'''
import shelve,os
hs =shelve.open('highscore.shlf')
def highscore(name, score):
    
    if name not in hs.keys() or hs[name] < score:
        hs[name] =score
        return hs[name]
    else:
        return hs[name];
    
def clr():
    hs.clear()
        