'''
Created on Jun 9, 2014

@author: hauyeung
'''
import unittest, highscore


class Test(unittest.TestCase):


    def test_shelve(self):
        highscore.highscore('John Doe',5)
        highscore.highscore('John Doe', 3)
        self.assertEqual(5, highscore.highscore('John Doe',3))
        highscore.clr()
        
    def test_multiplayer(self):
        highscore.highscore('Jane Doe',5)
        highscore.highscore('John Doe',3)
        highscore.highscore('Susan Smith',1)
        self.assertEqual(5, highscore.highscore('Jane Doe',5))
        highscore.clr()
    
    def test_tie(self):
        highscore.highscore('John Doe',10)
        highscore.highscore('Jane Smith',10)
        self.assertEqual(10, highscore.highscore('John Doe',10))
        highscore.clr()
        
    def test_neg(self):
        highscore.highscore('John Doe',10)
        highscore.highscore('Jane Smith',-10)
        self.assertEqual(10, highscore.highscore('John Doe',10))
        highscore.clr()
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_shelve']
    unittest.main()